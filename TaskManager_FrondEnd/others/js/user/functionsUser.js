//...................................User..............................................
/*this file contain the code necessary to make all the requests for users*/

//Initializer for details
$(function(){
  var $users = $('#users');

  var userTemplate = $('#user-template').html();

  function addUser (user){
    $users.append(Mustache.render(userTemplate, user));
  }

//get all users
  $.ajax({
    type: 'GET',
    url: 'http://localhost:3000/users',
    success: function(users) {
      $.each(users, function(i, user) {
        addUser(user);
      });
    },
    error: function() {
      alert('error loading users');
    }
  });


  //DELETE User
  //Method for deleting users
  $users.delegate('.remove', 'click', function() {
    var $tr = $(this).closest('tr');

    $.ajax({
      type: 'DELETE',
      url: 'http://localhost:3000/users/' + $(this).attr('data-id'),
      success: function (){
        //makes the fade effect
        $tr.fadeOut(300, function() {
          $(this).remove();
        });
      },
      error: function(jqXHR, textStatus, errorThrown) {
        if (jqXHR.status === 0) {
          alert('Not connect: Verify Network.');
        } else if (jqXHR.status == 404) {
          alert('Requested page not found [404]');
        } else if (jqXHR.status == 500) {
          alert('Internal Server Error [500].');
        } else if (textStatus === 'parsererror') {
          alert('Requested JSON parse failed.');
        } else if (textStatus === 'timeout') {
          alert('Time out error.');
        } else if (textStatus === 'abort') {
          alert('Ajax request aborted.');
        } else {
          alert('Uncaught Error: ' + jqXHR.responseText);
        }
      }
    });
  });


  //PUT task
  $users.delegate('.editUser', 'click', function() {
    debugger
    var $tr= $(this).closest('tr.editable');
    $tr.find('input.usernameValue').val($tr.find('span.usernameValue').html());
    $tr.find('input.complete-name').val($tr.find('span.complete-name').html());
    $tr.addClass('edit');
  });

  $users.delegate('.cancelEdit', 'click', function() {
    $(this).closest('tr.editable').removeClass('edit');
  });

  $users.delegate('.saveEdit', 'click', function() {
    debugger
    var $tr = $(this).closest('tr.editable');

    var username = $tr.find('input.usernameValue').val();
    var completeName = $tr.find('input.complete-name').val();
    var password = $tr.find('input.password').val();
    var confirmationPassword = $tr.find('input.confirmation-password').val();
    var role = $tr.find('select.role').val();
    var userId = $(this).attr('data-id');

    if (password == confirmationPassword) {
      $.ajax({
        method: "PUT",
        url: "http://localhost:3000/users/" + userId,
        data: {
          "username": username,
          "password": password,
          "complete_name": completeName,
          "role": role,
          "id": userId,
          "user": {
            "username": username,
            "password": password,
            "complete_name": completeName,
            "role": role
          }
        },
        dataType: "JSON"
      }).done(function(response){
        $tr.find('span.usernameValue').html(username);
        $tr.find('span.complete-name').html(completeName);
        $tr.find('span.role').html(role);
        $tr.removeClass('edit');
        alert("Task edited successfully");
      }).fail(function(error){
        alert("Error editing the task")
      });
    }else {
      alert('The Password and Confirmation Password must be the same');
    }
  });
});

//POST Users
//Method for posting Users
document.getElementById("createButton").addEventListener("click", function(){
  debugger
  var username = $("#username").val();
  var completeName = $("#complete-name").val();
  var password = $("#password").val();
  var confirmationPassword = $("#confirmation-password").val();
  var role = $("#role").val();
  if (password == confirmationPassword) {
    $.ajax({
      method: "POST",
      url: "http://localhost:3000/users",
      data: {
        "username": username,
        "password": password,
        "complete_name": completeName,
        "role": role,
        "user": {
          "username": username,
          "password": password,
          "complete_name": completeName,
          "role": role
        }
      },
      dataType: "JSON",
      success: function (){
        alert('User inserted successfully');
      },
      error: function(jqXHR, textStatus, errorThrown) {
        if (jqXHR.status === 0) {
          alert('Not connect: Verify Network.');
        } else if (jqXHR.status == 404) {
          alert('Requested page not found [404]');
        } else if (jqXHR.status == 500) {
          alert('Internal Server Error [500].');
        } else if (textStatus === 'parsererror') {
          alert('Requested JSON parse failed.');
        } else if (textStatus === 'timeout') {
          alert('Time out error.');
        } else if (textStatus === 'abort') {
          alert('Ajax request aborted.');
        } else {
          alert('Uncaught Error: ' + jqXHR.responseText);
        }
      }
    })
  }else {
    alert('The Password and Confirmation Password must be the same');
  }
});
