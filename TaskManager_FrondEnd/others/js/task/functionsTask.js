
//...................................Department..............................................
//initializer for modal details

$(function(){
  var $assignments= $('#assignments');
  var assignmentTemplate = $('#assignment-template').html();

  function addAssignment (assignment){
    $assignments.append(Mustache.render(assignmentTemplate, assignment));
  }

//get all departments
  $.ajax({
    type: 'GET',
    url: 'http://localhost:3000/assignments',
    success: function(assignments) {
      $.each(assignments, function(i, assignment) {
        addAssignment(assignment);
      });
    },
    error: function() {
      alert('error loading assignments');
    }
  });

/*
  //get department per id
  $departments.delegate('.edition', 'click', function() {
    $.ajax({
      type: 'GET',
      url: 'http://localhost:3000/departments/' + $(this).attr('data-id'),
      success: function(departments) {
        $.each(departments, function(i, department) {
          editDepartment(department);
        });
      },
      error: function() {
        alert('error loading department');
      }
    });
  });
*/


//DELETE task
  $assignments.delegate('.remove', 'click', function() {
    if (confirm("You are going to delete an Assignment. Do you want to continue??")) {
      var $div = $(this).closest('div.col-sm-3');
      $.ajax({
        type: 'DELETE',
        url: 'http://localhost:3000/assignments/' + $(this).attr('data-id'),
        success: function (){
          //makes the fade effect
          $div.fadeOut(300, function() {
            $(this).remove();
          });
        }
      });
    } else {
        //optional code
    }
  });


  //PUT task
  $assignments.delegate('.editAssignment', 'click', function() {
    var $div = $(this).closest('div.col-sm-3');
    //$div.find('select.priority').val($div.find('span.priority').html());
    //$div.find('select.state').val($div.find('span.state').html());
    $div.addClass('edit');
  });

  $assignments.delegate('.cancelEdit', 'click', function() {
    $(this).closest('div.col-sm-3').removeClass('edit');
  });

  $assignments.delegate('.saveEdit', 'click', function() {
    debugger
    var $div = $(this).closest('div.col-sm-3');

    var taskPriority = $div.find('select.priority').val();
    var taskState = $div.find('select.state').val();
    var taskId = $(this).attr('data-id');

    $.ajax({
      method: "PUT",
      url: "http://localhost:3000/assignments/" + taskId,
      data: {
        "state": taskState,
        "priority": taskPriority,
        "id": taskId,
        "assignment": {
          "state": taskState,
          "priority": taskPriority
        }
      },
      dataType: "JSON"
    }).done(function(response){
      $div.find('span.priority').html(taskPriority);
      $div.find('span.state').html(taskState);
      $div.removeClass('edit');
      alert("Task edited successfully");
    }).fail(function(error){
      alert("Error editing the task")
    });
  });

});

//POST Assignments
document.getElementById("createTaskButton").addEventListener("click", function(){
  debugger
//  window.alert("Desea continuar");
  var taskName = $("#titleTask").val();
  var taskState = $("#stateTask").val();
  var taskDate = $("#dateTask").val()+"T00:00:00.000Z";
  var taskDescription = $("#descriptionTask").val();
  var taskPriority = $("#priorityTask").val();

  $.ajax({
    method: "POST",
    url: "http://localhost:3000/assignments",

    data: {
      "title": taskName,
      "state": taskState,
      "task_date": taskDate,
      "description": taskDescription,
      "priority": taskPriority,
      "assignment": {
        "task_date": taskDate,
        "state": taskState,
        "description": taskDescription,
        "title": taskName,
        "priority": taskPriority
      }
    },
    dataType: "JSON"
  }).done(function(response){
    alert("Task added successfully");
  }).fail(function(error){
    alert("Error adding the task")
  });

});
