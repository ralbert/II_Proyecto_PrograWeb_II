//...................................Department..............................................
//initializer for modal details
$(function(){
  var $departments = $('#departments');

  var departmentTemplate = $('#department-template').html();

  function addDepartment (department){
    $departments.append(Mustache.render(departmentTemplate, department));
  }

//get all departments
  $.ajax({
    type: 'GET',
    url: 'http://localhost:3000/departments',
    success: function(departments) {
      $.each(departments, function(i, department) {
        addDepartment(department);
        selectLoad(department.code);
        //spanLoad(department.user_id);
        //updateTable();
      });
    },
    error: function() {
      alert('error loading departments');
    }
  });

  function spanLoad(userId){
    debugger
    var $tr= $(this).closest('tr.editable');
    var spanValue = "";
    $.ajax({
      type: 'GET',
      url: 'http://localhost:3000/users/' + userId,
      success: function(user) {
        debugger
        spanValue = user.complete_name;
        debugger

        //$tr.find('span.codeValue').html(spanValue);
        //$tr.find('span#'+userId).html(spanValue);
        $tr.find('.userId').html(spanValue);

      },
      error: function() {
        alert('error loading departments');
      }
    });
  }

  //get user roles
  //$departments.delegate('.editDepartment', 'click', function() {
  function selectLoad(departmentId){
    //var departmentId = $(this).attr('data-id');
    //var $users = $('#userRole');
    var $tr= $(this).closest('tr.editable');
    var completeNameVariable = "";
     $.ajax({
        type: 'GET',
        url: 'http://localhost:3000/users',
        success: function(users) {

          $("#example").empty();
          $.each(users, function(i, user) {

            if (user.role == "Chielf") {
              var option = "<option value='"+ user.id +"'>" + user.complete_name + "</option>";
              $("select#"+departmentId).append(option);

              debugger
              /*
              completeNameVariable = user.complete_name;
              $tr.find('span#'+departmentId).html(completeNameVariable);
*/
              //$users.append(option);
            }
          });
        },
        error: function() {
          alert('error loading departments');
        }
      });
    }
//  });
  //$users

  //PUT task
  $departments.delegate('.editDepartment', 'click', function() {
    var $tr= $(this).closest('tr.editable');
    $tr.find('input.codeValue').val($tr.find('span.codeValue').html());
    $tr.find('input.departmentNameValue').val($tr.find('span.departmentNameValue').html());
    $tr.addClass('edit');
  });

  $departments.delegate('.cancelEdit', 'click', function() {
    $(this).closest('tr.editable').removeClass('edit');
  });

  $departments.delegate('.saveEdit', 'click', function() {

    var $tr = $(this).closest('tr.editable');

    var departmentCode = $tr.find('input.codeValue').val();
    var departmentName = $tr.find('input.departmentNameValue').val();
    var departmentId = $(this).attr('data-id');
    var user_id = $tr.find('select.userId').val();;

    $.ajax({
      method: "PUT",
      url: "http://localhost:3000/departments/" + departmentId,
      data: {
        "code": departmentCode,
        "department_name": departmentName,
        "user_id": user_id,
        "id": departmentId,
        "department": {
          "code": departmentCode,
          "department_name": departmentName,
          "user_id": user_id
        }
      },
      dataType: "JSON"
    }).done(function(response){
      $tr.find('span.codeValue').html(departmentCode);
      $tr.find('span.departmentNameValue').html(departmentName);
      $tr.find('span.userId').html(user_id);
      $tr.removeClass('edit');
      alert("Department edited successfully");
    }).fail(function(error){
      alert("Error editing the task")
    });

  });

  //remove department
  $departments.delegate('.remove', 'click', function() {
    //here we are storing th tr for deleting it later
    var $tr = $(this).closest('tr');

    $.ajax({
      type: 'DELETE',
      url: 'http://localhost:3000/departments/' + $(this).attr('data-id'),
      success: function (){
        //makes the fade effect
        $tr.fadeOut(300, function() {
          $(this).remove();
        });
      }
    });
  });

});

//post departments
document.getElementById("createButton").addEventListener("click", function(){
  var departmentCode = $("#departmentCode").val();
  var departmentName = $("#departmentName").val();

  $.ajax({
    method: "POST",
    url: "http://localhost:3000/departments",
    data: {
      "code": departmentCode,
      "department_name": departmentName,
      "department": {
        "code": departmentCode,
        "department_name": departmentName
      }
    },
    dataType: "JSON"
  }).done(function(response){
    alert("agregado");
  }).fail(function(error){
    alert("error")
  });
});
