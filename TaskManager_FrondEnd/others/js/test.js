//...................................Department..............................................
//initializer for modal details
$(function(){
  var $departments = $('#departments');
  var departmentTemplate = $('#department-template').html();
  function addDepartment (department){
    $departments.append(Mustache.render(departmentTemplate, department));
  }

//get all departments
  $.ajax({
    type: 'GET',
    url: 'http://localhost:3000/departments',
    success: function(departments) {
      $.each(departments, function(i, department) {
        addDepartment(department);
      });
    },
    error: function() {
      alert('error loading departments');
    }
  });

//remove department
  $departments.delegate('.remove', 'click', function() {
    //here we are storing th tr for deleting it later
    var $tr = $(this).closest('tr');

    $.ajax({
      type: 'DELETE',
      url: 'http://localhost:3000/departments/' + $(this).attr('data-id'),
      success: function (){
        //makes the fade effect
        $tr.fadeOut(300, function() {
          $(this).remove();
        });
      }
    });
  });

});


/*
document.getElementById("editDepartment").addEventListener("click", function(){
  var $orders = $('#orders');
  //$departmentsEdition.delegate('.edition', 'click', function() {
  debugger
  $.ajax({
    type: 'GET',
    url: 'http://localhost:3000/departments',
    success: function(orders) {
      $.each(orders, function(i, order) {
        $orders.append('<li>Leegué</li>');
      });
    },
    error: function() {
      alert('error loading department');
    }
  });

});*/
//get department per id
/*
document.getElementById("editDepartment").addEventListener("click", function(){
  debugger
  var $departmentsEdition = $('#departmentsEdition');
//$departmentsEdition.delegate('.edition', 'click', function() {
  debugger
  $.ajax({
    type: 'GET',
    url: 'http://localhost:3000/departments/' + $(this).attr('data-id'),
    success: function(departments) {
      $.each(departments, function(i, department) {
        $departmentsEdition.append('<h1>Leegué</h1>');
      });
    },
    error: function() {
      alert('error loading department');
    }
  });
});

*/
//post departments

document.getElementById("createButton").addEventListener("click", function(){
  debugger
  var departmentCode = $("#departmentCode").val();
  var departmentName = $("#departmentName").val();

  $.ajax({
    method: "POST",
    url: "http://localhost:3000/departments",
    data: {
      "code": departmentCode,
      "department_name": departmentName,
      "department": {
        "code": departmentCode,
        "department_name": departmentName
      }
    },
    dataType: "JSON"
  }).done(function(response){
    alert("agregado");
  }).fail(function(error){
    alert("error")
  });
});
/*
$(function(){
  var $departments = $('#departments');

  var departmentTemplate = "<tr><td>{{code}}</td><td>{{department_name}}</td><td>"+
    "<button type='button' class='btn btn-outline-info' data-toggle='modal' data-target=''#modalDetail'>Show</button>"+
    "<button type='button' id='bntEditModal' class='btn btn-outline-warning' data-toggle='modal' data-target=''#modalEdit'>Edit</button>"+
    "<button type='button' data-id='{{id}}' class='btn btn-outline-danger remove' data-toggle='modal' data-target=''#modalDelete'>Delete</button>"+
    "</td></tr>"

  function addDepartment (department){
    $departments.append(Mustache.render(departmentTemplate, department));
  }

  $.ajax({
    type: 'GET',
    url: 'http://localhost:3000/departments',
    success: function(departments) {
      $.each(departments, function(i, department) {
        addDepartment(department);
      });
    },
    error: function() {
      alert('error loading departments');
    }
  });

  $departments.delegate('.remove', 'click', function() {
    //here we are storing th tr for deleting it later
    var $tr = $(this).closest('tr');

    $.ajax({
      type: 'DELETE',
      url: 'http://localhost:3000/departments/' + $(this).attr('data-id'),
      success: function (){
        //makes the fade effect
        $tr.fadeOut(300, function() {
          $(this).remove();
        });
      }
    });
  });

});

*/

/*
document.getElementById("createButton").addEventListener("click", function(){
  debugger
  var departmentCode = $("#code").val();
  var departmentName = $("#name").val();

  $.ajax({
    method: "POST",
    url: "http://localhost:3000/departments",
    data: {
      "code": departmentCode,
      "department_name": departmentName,
      "department": {
        "code": departmentCode,
        "department_name": departmentName
      }
    },


    dataType: "JSON"
  }).done(function(response){
    alert("agregado");
  }).fail(function(error){
    alert("error")
  });
});






$('#createButton').on('click', function() {
  var department = {
    code: $code.val(),
    department_name: $name.val(),
    department: {
      code: $code.val(),
      department_name: $name.val(),
    }
  };
  $.ajax({
    type: 'POST',
    url: 'http://localhost:3000/departments',
    data: department,
    success: function(newDepartment) {
      $departments.append('<li>code: '+ department.code +', Name: '+ department.department_name +'</li>')
    }
    error: function() {
      alert('error inserting department');
    }
  });
});
*/


/*

function addDepartment(department){
  $departments.append('<li>code: '+ department.code +', Name: '+ department.department_name +'</li>');
}

$(function(){
  var $departments = $('#departments');

  $.ajax({
    type: 'GET',
    url: 'http://localhost:3000/departments',
    success: function(departments) {
      $.each(departments, function(i, department) {
        addOrder(department);
      });
    },
    error: function() {
      alert('error loading departments');
    }
  });
});
*/
