class User < ApplicationRecord
  validates :username, presence: true


  before_create :hash_password
  has_many :sessions
  has_many :departments

  private
  def hash_password
    require 'digest/md5'
    self.password = Digest::MD5.hexdigest(password)
  end
end
