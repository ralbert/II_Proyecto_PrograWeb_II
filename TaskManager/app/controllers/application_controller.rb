class ApplicationController < ActionController::API
  private
  def validate_user
    #get the authentication token
    token = request.headers['Authorization']
    # if it does not exist, return a 401 status
    if token
      # if it exist check the value session
      session = Session.where(token: token).first
      #validate if it hasn't expired yet
      if !session
        # if not valid return 401
        render json: "{error: 'not a valid session'}".to_json, status: 401
      end
    else
      # if not valid return 401
      render json: "{error: 'not a valid session'}".to_json, status: :unprocessable_entity
    end
  end
end
