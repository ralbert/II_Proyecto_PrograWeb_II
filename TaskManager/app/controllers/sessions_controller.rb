class SessionsController < ApplicationController
  require 'digest/md5'
  # POST /sessions
  def create
    #@session = Session.new(session_params)
    #check if the user exist in the DB
    #byebug
    username = session_params[:username]
    password = session_params[:password]
    password = Digest::MD5.hexdigest(password)
    user = User.where(username: username, password: password).first
    if user
      #generate token
      token = Digest::MD5.hexdigest(username)
      #create the session
      session = Session.new(token: token)
      session.user_id = user.id
      if session.save
        #render json: @session, status: :created, location: @session
        # "username": "ralbert",
        render json: {"token": "#{token}", "role": "#{user.role}"}.to_json, status: :created
      else
        render json: @session.errors, status: :unprocessable_entity
      end
    end
  end

  # DELETE /sessions/1
  def destroy
    @session.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_session
      @session = Session.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def session_params
      #params.require(:session).permit(:username, :password)
      params.permit(:username, :password)
    end
end
