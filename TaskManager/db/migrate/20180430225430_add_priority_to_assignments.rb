class AddPriorityToAssignments < ActiveRecord::Migration[5.1]
  def change
    add_column :assignments, :priority, :string
  end
end
