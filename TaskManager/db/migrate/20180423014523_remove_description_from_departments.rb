class RemoveDescriptionFromDepartments < ActiveRecord::Migration[5.1]
  def change
    remove_column :departments, :description, :text
  end
end
