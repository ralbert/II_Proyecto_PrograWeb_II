class AddDepartmentIdToAssignments < ActiveRecord::Migration[5.1]
  def change
    add_reference :assignments, :department, foreign_key: true
  end
end
