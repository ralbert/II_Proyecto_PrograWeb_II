class AddDepartmentNameToDepartments < ActiveRecord::Migration[5.1]
  def change
    add_column :departments, :department_name, :string
  end
end
