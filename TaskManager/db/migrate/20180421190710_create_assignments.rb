class CreateAssignments < ActiveRecord::Migration[5.1]
  def change
    create_table :assignments do |t|
      t.datetime :task_date
      t.string :state

      t.timestamps
    end
  end
end
